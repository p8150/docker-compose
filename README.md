# docker-compose

to run on new host you need:
- $VAULT_ADDR
- $VAULT_TOKEN

```bash
docker run --pull always --rm -ti -h $HOSTNAME -v /var/run/docker.sock:/var/run/docker.sock -e VAULT_ADDR=$VAULT_ADDR -e VAULT_TOKEN=$VAULT_TOKEN registry.gitlab.com/p8150/docker-gitops:latest
```